

# Terraform Settings Block
terraform {
  required_version = ">=1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0" # Optional but recommended in production
    }
  }
}


# Provider Block
provider "aws" {
  profile = "terraform-user"  #Credentials Profile configured on your local desktop terminal  $HOME/.aws/credentials
  region  = "us-east-1"
}


## resource blocks
#creating EC2 instance
         #local name    #resource name
resource "aws_instance" "ec2_instance" {
  ami           = var.ami_id 
  instance_type = "t2.micro"
  vpc_security_group_ids =["sg-0a6b80c95cb010ed2"]

 tags = {
   Name = "EC2_Instance"
 }
}


# Create a VPC
# # resource "aws_vpc" "example" {
#   cidr_block = var.cidr_id
#   instance_tenancy = "default"

#     tags = {
#     Name = "example"
#   }
# }


#create vpc subnet
